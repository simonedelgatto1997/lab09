package it.unibo.oop.lab.reflectivefactory;


import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collector;
import java.util.stream.Collectors;

/**
 * Utility class for cloning objects via reflection.
 * 
 */
public final class ObjectClonerUtil {

    private static final String GETTER_PREFIX = "get";
    private static final String SETTER_PREFIX = "set";

    private ObjectClonerUtil() {

    }

    /**
     * Clone the object provided in input.
     * 
     * @param <T>
     *            the parametric class of the object to clone
     * @param obj
     *            the object to be cloned
     * @param theClass
     *            the class of the object to clone
     * @return the cloned object
     * @throws CloningException
     *             in case of any problem during the cloning
     * @throws IllegalAccessException
     * @throws InstantiationException
     */

    public static <T> T cloneObj(final T obj, final Class<T> theClass)
            throws CloningException, InstantiationException, IllegalAccessException {

        /*
         * 1) Retrieve the constructor from theClass
         */

        final T clone = theClass.newInstance();
        /*
         * 2) Cycle all the methods found in theClass. If the method is a getter (starts
         * with GETTER_PREFIX) of the current class (i.e. is not a getter defined in a
         * super class: use the getDeclaringClass method for this check) find the
         * corresponding setter, keeping in mind that: - The setter name can be
         * constructed by a combination of:
         * 
         * 1)SETTER_PREFIX and
         * 
         * 2) a substring of the getter method name.
         * 
         * Note: the getReturnType method of the class Method returns the return type of
         * a method
         */

        //FORSE QUESTO ERA MEGLIO FARLO PRIVATO
        List<Method> getters = Arrays.asList(theClass.getMethods()).stream()
                .filter(t -> t.getDeclaringClass() == theClass).filter(t -> t.getName().contains(GETTER_PREFIX))
                .collect(Collectors.toList());


        //FORSE ANCHE QUESTO ERA MEGLIO FARLO PRIVATO PER UNA MAGGIORE CHIAREZZA
        List<Method> setter = Arrays.asList(theClass.getMethods()).stream()
                .filter(t -> t.getDeclaringClass() == theClass).filter(t -> t.getName().contains(SETTER_PREFIX))
                .collect(Collectors.toList());


        Map<Method, Method> getterAndSetter = new HashMap<Method, Method>();
        for (Method g : getters) {
            String typeGetter = g.getName().substring(g.getName().lastIndexOf("get"));
            typeGetter = typeGetter.substring(3);
            final String typeGetter2 = typeGetter;
            System.out.println(typeGetter2);
            setter.stream().filter(s -> s.getName().contains(SETTER_PREFIX + typeGetter2))
                    .forEach(s -> getterAndSetter.put(g, s));
        }

//        getters.stream()
//                .forEach(g -> setter.stream()
//                        .filter(s -> s.getName().contains(
//                                SETTER_PREFIX + g.getName().substring(g.getName().lastIndexOf("get")).substring(3)))
//                        .forEach(t -> getterAndSetter.put(g, t)));

        System.out.println(getterAndSetter);

        /*
         * 3) Invoke the getter on the original object, obtain the value, and pass it to
         * the corresponding setter of the cloned object.
         */
        getterAndSetter.forEach((g, s) -> {
            try {
                s.invoke(clone, g.invoke(obj));
            } catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
                e.printStackTrace();
            }
        });
        /*
         * 6) Return the cloned object
         */
        return clone;
    }

    /**
     * @SuppressWarnings
     * @param args
     * 
     *            main of the class
     * 
     */
    @SuppressWarnings("unchecked")
    public static void main(final String[] args)
            throws CloningException, InstantiationException, IllegalAccessException {
        ClonableClass object = new ClonableClass();
        object.setA("ciao");
        object.setB("ciao");
        object.setD(123.0);
        ClonableClass obj2 = cloneObj(object, (Class<ClonableClass>) object.getClass());
        System.out.println(object.toString());
        System.out.println(obj2.toString());
    }
}