package it.unibo.oop.lab.lambda.ex02;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Optional;
import java.util.OptionalDouble;
import java.util.Set;
import java.util.stream.Stream;

/**
 *
 */
public class MusicGroupImpl implements MusicGroup {

    private final Map<String, Integer> albums = new HashMap<>();
    private final Set<Song> songs = new HashSet<>();

    @Override
    public void addAlbum(final String albumName, final int year) {
        this.albums.put(albumName, year);
    }

    @Override
    public void addSong(final String songName, final Optional<String> albumName, final double duration) {
        if (albumName.isPresent() && !this.albums.containsKey(albumName.get())) {
            throw new IllegalArgumentException("invalid album name");
        }
        this.songs.add(new MusicGroupImpl.Song(songName, albumName, duration));
    }

    @Override
    public Stream<String> orderedSongNames() {

        return this.songs.stream().map(t -> t.getSongName()).sorted(String::compareTo);
    }

    @Override
    public Stream<String> albumNames() {

        return this.albums.keySet().stream();
    }

    @Override
    public Stream<String> albumInYear(final int year) {
        Set<String> albumInYears = new HashSet<>();
        this.albums.forEach((k, v) -> {
            if (v.equals((Integer) year)) {
                albumInYears.add(k);
            }
        });
        return albumInYears.stream();
    }

    @Override
    public int countSongs(final String albumName) {
        return (int) this.songs.stream().filter(t -> t.getAlbumName().equals(Optional.ofNullable(albumName))).count();
    }

    @Override
    public int countSongsInNoAlbum() {
        return (int) this.songs.stream().filter(t -> !t.getAlbumName().isPresent()).count();
    }

    @Override
    public OptionalDouble averageDurationOfSongs(final String albumName) {
        return this.songs.stream().filter(t -> t.getAlbumName().equals(Optional.of(albumName)))
                .mapToDouble(t -> t.getDuration()).average();
    }

    @Override
    public Optional<String> longestSong() {
        return Optional.of(this.songs.stream()
                .max((t1, t2) -> (new Double(t1.getDuration()).compareTo(new Double(t2.getDuration())))).get()
                .getSongName());
    }

    @Override
    public Optional<String> longestAlbum() {
        Optional<String> result = Optional.empty();
        double resultValue = 0;
        double intermediumValue = 0;
        for (String s : this.albums.keySet()) {
            intermediumValue = this.songs.stream().filter(t -> t.getAlbumName().equals(Optional.of(s)))
                    .mapToDouble(t -> t.getDuration()).sum();
            if (!result.isPresent() || intermediumValue >= resultValue) {
                result = Optional.of(s);
                resultValue = intermediumValue;
            }
        }
        return result;

    }

    private static final class Song {

        private final String songName;
        private final Optional<String> albumName;
        private final double duration;
        private int hash;

        Song(final String name, final Optional<String> album, final double len) {
            super();
            this.songName = name;
            this.albumName = album;
            this.duration = len;
        }

        public String getSongName() {
            return songName;
        }

        public Optional<String> getAlbumName() {
            return albumName;
        }

        public double getDuration() {
            return duration;
        }

        @Override
        public int hashCode() {
            if (hash == 0) {
                hash = songName.hashCode() ^ albumName.hashCode() ^ Double.hashCode(duration);
            }
            return hash;
        }

        @Override
        public boolean equals(final Object obj) {
            if (obj instanceof Song) {
                final Song other = (Song) obj;
                return albumName.equals(other.albumName) && songName.equals(other.songName)
                        && duration == other.duration;
            }
            return false;
        }

        @Override
        public String toString() {
            return "Song [songName=" + songName + ", albumName=" + albumName + ", duration=" + duration + "]";
        }

    }

}
