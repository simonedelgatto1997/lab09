package it.unibo.oop.lab.lambda.ex03;

import java.awt.BorderLayout;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.LayoutManager;
import java.awt.Toolkit;
import java.util.function.Function;
import java.util.stream.Collectors;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextArea;

import java.util.*;

/**
 * Modify this small program adding new filters.
 * 
 * 1) Convert to lower case
 * 
 * 2) Count the number of chars
 * 
 * 3) Count the number of lines
 * 
 * 4) List all the words in alphabetical order
 * 
 * 5) Write the count for each word, e.g. "word word pippo" should output "pippo
 * -> 1 word -> 2
 *
 */

public final class LambdaFilter extends JFrame {

    private static final long serialVersionUID = 1760990730218643730L;

    private enum Command {
        IDENTITY("No modifications", Function.identity()), NLINES("Number of lines",
                t -> String.valueOf(t.split("\n").length)), NOFWORD("Numbers of word", t -> {
                    String string = "";
                    List<String> list = Arrays.asList(t.split("\\s"));
                    System.out.print(list);
                    Map<String, Integer> map = list.stream().filter(z -> !z.contains("\\n"))
                            .filter(z -> !z.contains("\r")).collect(Collectors.toMap(x -> x, x -> 1, (x, y) -> x + y));
                    for (Map.Entry<String, Integer> s : map.entrySet()) {
                        string = string.concat(s.getKey() + ": " + s.getValue() + " ");
                    }
                    System.out.println(string);
                    return string;

                    // String s = "";
                    // Arrays.asList(t.trim().split("\\s")).stream().filter(t -> !t.equals("\n"))
                    // .collect(Collectors.toMap(x -> x, x -> 1, (x, y) -> x + y))// forEach((x,y)
                    // ->
                    // // System.out.println(x + y));
                    // .forEach((x, y) -> s.concat(x + ": " + y + " "));
                    // System.out.println(s);
                    // return s;
                });

        private final String commandName;
        private final Function<String, String> fun;

        Command(final String name, final Function<String, String> process) {
            commandName = name;
            fun = process;
        }

        @Override
        public String toString() {
            return commandName;
        }

        public String translate(final String s) {
            return fun.apply(s);
        }
    }

    private LambdaFilter() {
        super("Lambda filter GUI");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        final JPanel panel1 = new JPanel();
        final LayoutManager layout = new BorderLayout();
        panel1.setLayout(layout);
        final JComboBox<Command> combo = new JComboBox<>(Command.values());
        panel1.add(combo, BorderLayout.NORTH);
        final JPanel centralPanel = new JPanel(new GridLayout(1, 2));
        final JTextArea left = new JTextArea();
        left.setBorder(BorderFactory.createLineBorder(Color.BLACK));
        final JTextArea right = new JTextArea();
        right.setBorder(BorderFactory.createLineBorder(Color.BLACK));
        right.setEditable(false);
        centralPanel.add(left);
        centralPanel.add(right);
        panel1.add(centralPanel, BorderLayout.CENTER);
        final JButton apply = new JButton("Apply");
        // apply.addActionListener(ev -> right.setText(((Command)
        // combo.getSelectedItem()).translate(left.getText())));
        apply.addActionListener(ev -> right.setText(((Command) combo.getSelectedItem()).translate(left.getText())));
        panel1.add(apply, BorderLayout.SOUTH);
        setContentPane(panel1);
        final Dimension screen = Toolkit.getDefaultToolkit().getScreenSize();
        final int sw = (int) screen.getWidth();
        final int sh = (int) screen.getHeight();
        setSize(sw / 4, sh / 4);
        setLocationByPlatform(true);
    }

    /**
     * @param a
     *            unused
     */
    public static void main(final String... a) {
        final LambdaFilter gui = new LambdaFilter();
        gui.setVisible(true);
    }

}
